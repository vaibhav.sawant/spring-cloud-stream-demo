package com.tw.demo.listner;

import com.tw.demo.channels.MessageChannels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EventListener {

    @StreamListener(value = MessageChannels.TRANSACTION_INPUT)
    public void transactionListener(Double amount) {
        log.info("Received message with amount: {}", amount);
    }

    @StreamListener(value = MessageChannels.FEE_INPUT)
    @SendTo(MessageChannels.PROCESSED_FEE_OUTPUT)
    public Double feeTransactionListener(Double amount) {
        log.info("Transforming fee with amount: {}", amount);
        return amount.doubleValue() * 10;
    }
}
