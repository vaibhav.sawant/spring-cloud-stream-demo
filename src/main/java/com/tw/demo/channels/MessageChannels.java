package com.tw.demo.channels;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MessageChannels {

    String TRANSACTION_INPUT = "transactionInput";
    String TRANSACTION_OUTPUT = "transactionOutput";

    String FEE_INPUT = "feeInput";
    String FEE_OUTPUT = "feeOutput";
    String PROCESSED_FEE_OUTPUT = "processedFeeOutput";

    @Input
    SubscribableChannel transactionInput();

    @Output(TRANSACTION_OUTPUT)
    MessageChannel transactionOutput();

    @Input
    SubscribableChannel feeInput();

    @Output(FEE_OUTPUT)
    MessageChannel feeOutput();

    @Output(PROCESSED_FEE_OUTPUT)
    MessageChannel processedFeeOutput();
}
