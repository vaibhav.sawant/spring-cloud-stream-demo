package com.tw.demo.controller;

import com.tw.demo.builder.MessageBuilder;
import com.tw.demo.channels.MessageChannels;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("publish")
@AllArgsConstructor
public class EventPublisherController {

    private MessageChannels messageChannels;

    @PostMapping("transaction")
    public String postTransaction(@RequestParam(required = false, defaultValue = "100.00") Double transactionAmount) {
        log.info("Publishing event for transactionAmount: {}", transactionAmount);

        messageChannels
            .transactionOutput()
            .send(MessageBuilder.createMessage(transactionAmount));

        return String.format("Message published with transactionAmount %s", transactionAmount);
    }

    @PostMapping("fee")
    public String postFeeTransaction(@RequestParam(required = false, defaultValue = "150.00") Double transactionAmount) {
        log.info("Publishing Fee event for transactionAmount: {}", transactionAmount);

        messageChannels
            .feeOutput()
            .send(MessageBuilder.createMessage(transactionAmount));

        return String.format("Fee message published with transactionAmount %s", transactionAmount);
    }
}
